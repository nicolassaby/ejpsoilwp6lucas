
# EJP Soil WP6 Task 6.3 : comparison of soil monitoring network dataset 

 ![](img/EJP_Soil_logo_cmyk-h100.png)

<img src="img/Logo INRAE 1600x600.jpg" width="200" />


EJP SOIL is a European Joint Programme Cofund on Agricultural Soil Management contributing to key societal challenges including climate change, water and future food security.  

The objectives are to develop knowledge, tools and an integrated research community to foster climate-smart sustainable agricultural soil management that:

Allows sustainable food production
Sustains soil biodiversity
Sustains soil functions that preserves ecosystem services  

In task 6.3, all partners involved in WP6 of the EJP SOIL also agreed in comparing their datasets with those from LUCAS. Few countries already have started such studies to compare distributions, means, spatial representations, statistical designs of both their soil monitoring system with LUCAS . When possible, to harmonise depths we may test for instance spline functions, which will add error but make comparable the depths. This may also help in designing transfer functions between their results and LUCAS ones. As previously a common way of testing will be agreed. 
 

## Data
The data correspond to the lucas dataset and RMQS french dataset (https://doi.org/10.15454/QSXKGA)

## Code
The code used to compare the two datasets is the code named `comparison_LUCAS_RMQS.Rmd`

A online knitted version of the code is available following  [this link](https://nicolassaby.pages.mia.inra.fr/ejpsoilwp6lucas/).






